from envs.gym_wrapper import *
from utils.data_management import *
from moral.ppo_train import *
from utils.generate_demos import *
from moral.airl_train import *
from moral.moral_train import *


# folder to load config file
CONFIG_PATH = "configs/"
CONFIG_FILENAME_0 = "ppo.yaml"
CONFIG_FILENAME_1 = "generate_demos.yaml"
CONFIG_FILENAME_2 = "airl.yaml"
CONFIG_FILENAME_3 = "moral.yaml"
PATHS_CONFIG_FILENAME = "paths.yaml"

if __name__ == '__main__':

    paths_config = load_config(CONFIG_PATH, PATHS_CONFIG_FILENAME)

    # Check that every configuration file correspond to the same training process (same environment, experts weights, etc.)

    # Step 0 : Train ppo agents
    c_0 = load_config(CONFIG_PATH, CONFIG_FILENAME_0)
    experts_filenames, demos_filenames, generators_filenames, discriminators_filenames = paths_management(paths_config, c_0)
    for i in range(c_0["nb_experts"]):
        ppo_train_1_expert(c_0, experts_filenames[i])

    # Step 1 : Generate demonstrations
    c_1 = load_config(CONFIG_PATH, CONFIG_FILENAME_1)
    experts_filenames, demos_filenames, generators_filenames, discriminators_filenames = paths_management(paths_config, c_1)
    for i in range(c_1["nb_experts"]):
        generate_demos_1_expert(c_1["env_id"], c_1["nb_demos"], experts_filenames[i], demos_filenames[i])

    # Step 2 : Train airl (generators and discriminators)
    c_2 = load_config(CONFIG_PATH, CONFIG_FILENAME_2)
    experts_filenames, demos_filenames, generators_filenames, discriminators_filenames = paths_management(paths_config, c_2)
    for i in range(c_2["nb_experts"]):
        airl_train_1_expert(c_2, demos_filenames[i], generators_filenames[i], discriminators_filenames[i], prints=False)

    # Step 3 : Train moral agent
    c_3 = load_config(CONFIG_PATH, CONFIG_FILENAME_3)
    experts_filenames, demos_filenames, generators_filenames, discriminators_filenames = paths_management(paths_config, c_3)
    moral_filename, non_eth_expert = moral_paths_management(paths_config, c_3)
    query_freq = c_3["query_freq"]
    if c_3["real_params"]:
        env_steps = int(c["env_steps"]/c_3["n_workers"])
        query_freq = int(env_steps/(c_3["n_queries"]+2))
    moral_train_n_experts(c_3, query_freq, env_steps, generators_filenames, discriminators_filenames, moral_filename, non_eth_expert)