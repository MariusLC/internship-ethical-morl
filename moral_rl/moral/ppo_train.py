import torch
from tqdm import tqdm
import wandb
import argparse

from envs.gym_wrapper import *
from utils.data_management import *
from moral.ppo import *

# Use GPU if available
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

def ppo_train_1_expert(c, filename):

    # Init WandB & Parameters
    wandb.init(project='PPO', 
        config=c,
        reinit=True)
    config = wandb.config

    # Create Environment
    vec_env = VecEnv(config.env_id, config.n_workers)
    states = vec_env.reset()
    states_tensor = torch.tensor(states).float().to(device)

    # Fetch Shapes
    n_actions = vec_env.action_space.n
    obs_shape = vec_env.observation_space.shape
    state_shape = obs_shape[:-1]
    in_channels = obs_shape[-1]

    # Initialize Models
    ppo = PPO(state_shape=state_shape, in_channels=in_channels, n_actions=n_actions).to(device)
    optimizer = torch.optim.Adam(ppo.parameters(), lr=config.lr_ppo)
    dataset = TrajectoryDataset(batch_size=config.batchsize_ppo, n_workers=config.n_workers)

    for t in tqdm(range(int(config.env_steps / config.n_workers))):
        actions, log_probs = ppo.act(states_tensor)
        next_states, rewards, done, info = vec_env.step(actions)
        scalarized_rewards = [sum([config.expert_weights[i] * r[i] for i in range(len(r))]) for r in rewards]

        train_ready = dataset.write_tuple(states, actions, scalarized_rewards, done, log_probs, rewards, gamma=config.gamma)

        if train_ready:
            update_policy(ppo, dataset, optimizer, config.gamma, config.epsilon, config.ppo_epochs, config.entropy_reg)
            # update_policy_v3(ppo, dataset, optimizer, config.gamma, config.epsilon, config.ppo_epochs, config.entropy_reg, wandb)
            # Log Objectives
            objective_logs = dataset.log_objectives()
            objective_logs = np.mean(objective_logs, axis=0)
            for i, obj in enumerate(objective_logs):
                wandb.log({'Obj_' + str(i): obj}, step=t*config.n_workers)
            for i, ret in enumerate(dataset.log_rewards()):
                wandb.log({'Returns': ret}, step=(t//config.n_workers)*config.n_workers+i)
            wandb.log({'Returns mean': np.mean(dataset.log_rewards())}, step=t*config.n_workers)
            dataset.reset_trajectories()

        # Prepare state input for next time step
        states = next_states.copy()
        states_tensor = torch.tensor(states).float().to(device)

        # vec_env.close()
        save_data(ppo, filename)

def test(c, filename):
    filename += "TEST"

    # Init WandB & Parameters
    wandb.init(project='PPO', 
        config=c,
        reinit=True)
    config = wandb.config

    # Create Environment
    vec_env = VecEnv(config.env_id, config.n_workers)
    states = vec_env.reset()
    states_tensor = torch.tensor(states).float().to(device)

    # Fetch Shapes
    n_actions = vec_env.action_space.n
    obs_shape = vec_env.observation_space.shape
    state_shape = obs_shape[:-1]
    in_channels = obs_shape[-1]

    # Initialize Models
    ppo = PPO(state_shape=state_shape, in_channels=in_channels, n_actions=n_actions).to(device)
    optimizer = torch.optim.Adam(ppo.parameters(), lr=config.lr_ppo)
    dataset = TrajectoryDataset(batch_size=config.batchsize_ppo, n_workers=config.n_workers)
    save_data(ppo, filename)

# folder to load config file
CONFIG_PATH = "configs/"
CONFIG_FILENAME = "ppo.yaml"
PATHS_CONFIG_FILENAME = "paths.yaml"

if __name__ == '__main__':

    c = load_config(CONFIG_PATH, CONFIG_FILENAME)
    paths_config = load_config(CONFIG_PATH, PATHS_CONFIG_FILENAME)

    expert_filename, _, _, _ = paths_management(paths_config, c)
    
    # expert_filename += "TEST"
    ppo_train_1_expert(c, expert_filename)
    # test(c, expert_filename)
