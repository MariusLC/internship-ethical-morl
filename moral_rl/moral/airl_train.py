import wandb
import pickle
import argparse
import torch
import numpy as np
from tqdm import tqdm
from stable_baselines3.common.vec_env import SubprocVecEnv

from envs.gym_wrapper import *
from moral.ppo import *
from moral.ppo import PPO, TrajectoryDataset, update_policy
from drlhp.preference_model import *
from moral.preference_giver import *
from utils.data_management import *
# from utils.evaluate_ppo import *
from moral.airl import *

# Device Check
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

def airl_train_1_expert(c, demos_filename, generator_filename, discriminator_filename, prints=False):

    # Load demonstrations
    expert_trajectories = pickle.load(open(demos_filename, 'rb'))

    # Init WandB & Parameters
    wandb.init(
        project='AIRL',
        config=c, 
        reinit=True)
    config = wandb.config

    # Create Environment
    vec_env = SubprocVecEnv([make_env(config.env_id, i) for i in range(config.n_workers)])
    states = vec_env.reset()
    states_tensor = torch.tensor(states).float().to(device)

    # Fetch Shapes
    n_actions = vec_env.action_space.n
    obs_shape = vec_env.observation_space.shape
    state_shape = obs_shape[:-1]
    in_channels = obs_shape[-1]

    # Initialize Models
    ppo = PPO(state_shape=state_shape, n_actions=n_actions, in_channels=in_channels).to(device)
    discriminator = Discriminator(state_shape=state_shape, in_channels=in_channels).to(device)
    optimizer = torch.optim.Adam(ppo.parameters(), lr=5e-4)
    optimizer_discriminator = torch.optim.Adam(discriminator.parameters(), lr=5e-5)
    dataset = TrajectoryDataset(batch_size=config.batchsize_ppo, n_workers=config.n_workers)

    # Logging
    objective_logs = []

    for t in tqdm(range((int(config.env_steps/config.n_workers)))):

        # Act
        actions, log_probs = ppo.act(states_tensor)
        next_states, rewards, done, info = vec_env.step(actions)

        # Log Objectives
        objective_logs.append(rewards)

        # Calculate (vectorized) AIRL reward
        airl_state = torch.tensor(states).to(device).float()
        airl_next_state = torch.tensor(next_states).to(device).float()
        airl_action_prob = torch.exp(torch.tensor(log_probs)).to(device).float()
        # airl_rewards = discriminator.predict_reward_2(airl_state, airl_next_state, config.gamma, airl_action_prob)
        airl_advantages, airl_rewards = discriminator.predict_reward_2(airl_state, airl_next_state, config.gamma, airl_action_prob)
        airl_rewards = list(airl_rewards.detach().cpu().numpy() * [0 if i else 1 for i in done])
        airl_advantages = list(airl_advantages.detach().cpu().numpy() * [0 if i else 1 for i in done])

        # Save Trajectory
        train_ready = dataset.write_tuple(states, actions, airl_rewards, done, log_probs)
        # train_ready = dataset.write_tuple_2(states, actions, airl_rewards, airl_advantages, done, log_probs)

        if train_ready:
            # Log Objectives
            objective_logs = np.array(objective_logs).sum(axis=0)
            objective_logs = np.mean(objective_logs, axis=0)
            for i, obj in enumerate(objective_logs):
                wandb.log({'Obj_' + str(i): obj}, step=t*config.n_workers)
            objective_logs = []


            # Update Models
            update_policy(ppo, dataset, optimizer, config.gamma, config.epsilon, config.ppo_epochs,
                          entropy_reg=config.entropy_reg)
            d_loss, fake_acc, real_acc = update_discriminator(discriminator=discriminator,
                                                              optimizer=optimizer_discriminator,
                                                              gamma=config.gamma,
                                                              expert_trajectories=expert_trajectories,
                                                              policy_trajectories=dataset.trajectories.copy(), ppo=ppo,
                                                              batch_size=config.batchsize_discriminator)

            if (prints):
                print('Discriminator Loss ', d_loss)
                print('Fake Accuracy ', fake_acc)
                print('Real Accuracy ', real_acc)

                print("mean discrim rew = ", sum(dataset.log_returns()))
                print("mean discrim adv = ", sum(dataset.log_advantages()))

                mean_ppo, std_ppo = evaluate_ppo(ppo, config)
                print("Mean returns per traj : ", mean_ppo)
                print("Std returns per traj : ", std_ppo)

            # Log Loss Statsitics
            wandb.log({'Discriminator Loss': d_loss,
                       'Fake Accuracy': fake_acc,
                       'Real Accuracy': real_acc}, step=t*config.n_workers)
            for i, ret in enumerate(dataset.log_rewards()):
                wandb.log({'Returns': ret}, step=(t//config.n_workers)*config.n_workers+i)
            wandb.log({'Returns mean': np.mean(dataset.log_rewards())}, step=t*config.n_workers)

            dataset.reset_trajectories()


            # SAVE THE DISCRIMINATOR FOR THE MORAL STEP
            # torch.save(discriminator.state_dict(), discriminator_filename)
            save_data(discriminator, discriminator_filename)

            # SAVE THE GENERATOR FOR THE MORAL STEP ?
            # torch.save(ppo.state_dict(), generator_filename)
            save_data(ppo, generator_filename)

        # Prepare state input for next time step
        states = next_states.copy()
        states_tensor = torch.tensor(states).float().to(device)

        #vec_env.close()

def test(c, demos_filename, generator_filename, discriminator_filename, prints=False):
    # Load demonstrations
    expert_trajectories = pickle.load(open(demos_filename, 'rb'))

    # Init WandB & Parameters
    wandb.init(
        project='AIRL',
        config=c, 
        reinit=True)
    config = wandb.config

    # Create Environment
    vec_env = SubprocVecEnv([make_env(config.env_id, i) for i in range(config.n_workers)])
    states = vec_env.reset()
    states_tensor = torch.tensor(states).float().to(device)

    # Fetch Shapes
    n_actions = vec_env.action_space.n
    obs_shape = vec_env.observation_space.shape
    state_shape = obs_shape[:-1]
    in_channels = obs_shape[-1]

    # Initialize Models
    ppo = PPO(state_shape=state_shape, n_actions=n_actions, in_channels=in_channels).to(device)
    discriminator = Discriminator(state_shape=state_shape, in_channels=in_channels).to(device)
    optimizer = torch.optim.Adam(ppo.parameters(), lr=5e-4)
    optimizer_discriminator = torch.optim.Adam(discriminator.parameters(), lr=5e-5)
    dataset = TrajectoryDataset(batch_size=config.batchsize_ppo, n_workers=config.n_workers)

    save_data(discriminator, discriminator_filename)
    save_data(ppo, generator_filename)


# folder to load config file
CONFIG_PATH = "configs/"
CONFIG_FILENAME = "airl.yaml"
PATHS_CONFIG_FILENAME = "paths.yaml"

if __name__ == '__main__':

    c = load_config(CONFIG_PATH, CONFIG_FILENAME)
    paths_config = load_config(CONFIG_PATH, PATHS_CONFIG_FILENAME)

    expert_filename, demos_filename, generator_filename, discriminator_filename = paths_management(paths_config, c)
    
    # AIRL RUN
    airl_train_1_expert(c, demos_filename, generator_filename, discriminator_filename, prints=False)

    # TEST
    # generator_filename += "TEST"
    # discriminator_filename += "TEST"
    # test(c, demos_filename, generator_filename, discriminator_filename, prints=False)