import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from envs.gym_wrapper import *

from tqdm import tqdm
import torch
import numpy as np
import matplotlib.pyplot as plt
import wandb
import argparse
import yaml
import os

import numpy as np
import math
import scipy.stats as st

from moral.active_learning import *
from moral.preference_giver import *
from utils.generate_demos import *
from utils.data_management import *
from moral.airl import *
from moral.ppo import *


def evaluate_from_demos(demos):
    res = np.array([np.array(demo["returns"]).sum(axis=0) for demo in demos])
    return res.mean(axis=0), res.std(axis=0)

def evaluate_ppo(ppo, env_id, n_eval=1000):
    """
    :param ppo: Trained policy
    :param env_id: Environment
    :param n_eval: Number of evaluation steps
    :return: mean, std of rewards
    """
    env = GymWrapper(env_id)
    states = env.reset()
    states_tensor = torch.tensor(states).float().to(device)

    obj_logs = []
    obj_returns = []

    actions_chosen = np.zeros(10)

    for t in tqdm(range(n_eval)):
        actions, log_probs = ppo.act(states_tensor)
        next_states, reward, done, info = env.step(actions)
        obj_logs.append(reward)

        actions_chosen[actions] += 1

        if done:
            next_states = env.reset()
            obj_logs = np.array(obj_logs).sum(axis=0)
            obj_returns.append(obj_logs)
            obj_logs = []

        # Prepare state input for next time step
        states = next_states.copy()
        states_tensor = torch.tensor(states).float().to(device)

    obj_returns = np.array(obj_returns)
    obj_means = obj_returns.mean(axis=0)
    obj_std = obj_returns.std(axis=0)

    print("action chosen = ", actions_chosen)
    return list(obj_means), list(obj_std)


# folder to load config file
CONFIG_PATH = "configs/"
CONFIG_FILENAME = "evaluate_agent.yaml"

if __name__ == '__main__':

	c = load_config(CONFIG_PATH, CONFIG_FILENAME)

	# Create Environment
	vec_env = VecEnv(c["env_id"], 12)
	states = vec_env.reset()
	states_tensor = torch.tensor(states).float().to(device)

	# Fetch Shapes
	n_actions = vec_env.action_space.n
	obs_shape = vec_env.observation_space.shape
	state_shape = obs_shape[:-1]
	in_channels = obs_shape[-1]

	if c["mode"] == "agent" :
		expert_filename = c["agent_filename"]
		expert_policy = PPO(state_shape=state_shape, in_channels=in_channels, n_actions=n_actions).to(device)
		expert_policy.load_state_dict(torch.load(expert_filename, map_location=torch.device('cpu')))
		mean, std = evaluate_ppo(expert_policy, c["env_id"], c["nb_steps"])

	elif c["mode"] == "demos" :
		demos = pickle.load(open(c["demos_filename"], 'rb'))
		mean, std = evaluate_from_demos(demos)

	mean_roud = [round(r, 2) for r in mean]
	print("eval expert = ", mean_roud)