import os 
import torch
import pickle
import yaml


def save_data(data, filename):
    path = filename.split("/")
    path = path[:-1]
    path = "/".join(path)
    if not os.path.exists(path):
        os.makedirs(path)
    torch.save(data.state_dict(), filename)

def save_demos(dataset, demos_filename):
    path = demos_filename.split("/")
    path = path[:-1]
    path = "/".join(path)
    if not os.path.exists(path):
        os.makedirs(path)
    pickle.dump(dataset, open(demos_filename, 'wb'))

# Function to load yaml configuration file
def load_config(config_path, config_name):
    with open(os.path.join(config_path, config_name)) as file:
        config = yaml.safe_load(file)

    return config

def paths_management(paths_config, c):
    vanilla_path = ""
    if c["vanilla"]:
        vanilla_path = paths_config["vanilla_path"]
    if c["nb_experts"] == 1:
        path = paths_config["data_path"]+c["env"]+"/"+vanilla_path+str(c["expert_weights"])+"/"
        experts_filenames = path+paths_config["expe_path"]+paths_config["model_ext"]
        demos_filenames = path+paths_config["demo_path"]+paths_config["demo_ext"]
        generators_filenames = path+paths_config["gene_path"]+paths_config["model_ext"]
        discriminators_filenames = path+paths_config["disc_path"]+paths_config["model_ext"]
    else : 
        experts_filenames = []
        demos_filenames = []
        generators_filenames = []
        discriminators_filenames = []
        for i, w in enumerate(c["experts_weights"]):
            path = paths_config["data_path"]+c["env"]+"/"+vanilla_path+str(w)+"/"
            experts_filenames.append(path+paths_config["expe_path"]+paths_config["model_ext"])
            demos_filenames.append(path+paths_config["demo_path"]+paths_config["demo_ext"])
            generators_filenames.append(path+paths_config["gene_path"]+paths_config["model_ext"])
            discriminators_filenames.append(path+paths_config["disc_path"]+paths_config["model_ext"])

    return experts_filenames, demos_filenames, generators_filenames, discriminators_filenames

def paths_management_generate_demos_moral_agent(paths_config, c):
    vanilla_path = ""
    if c["vanilla"]:
        vanilla_path = paths_config["vanilla_path"]
    discriminators_filenames = []
    generators_filenames = []
    non_eth_expert = paths_config["data_path"]+c["env"]+"/"+vanilla_path+str(c["non_eth_experts_weights"])+"/"+paths_config["expe_path"]+paths_config["model_ext"]
    for i, w in enumerate(c["experts_weights"]):
        path = paths_config["data_path"]+c["env"]+"/"+vanilla_path+str(w)+"/"
        generators_filenames.append(path+paths_config["gene_path"]+paths_config["model_ext"])
        discriminators_filenames.append(path+paths_config["disc_path"]+paths_config["model_ext"])
    return non_eth_expert, generators_filenames, discriminators_filenames

def moral_paths_management(paths_config, c, version=0):
    vanilla_path = ""
    if c["vanilla"]:
        vanilla_path = paths_config["vanilla_path"]
    version_name = ""
    if version == 1:
        version_name = "moral_action_2_"
    moral_filename = paths_config["data_path"]+c["env"]+"/"+vanilla_path+paths_config["moral_path"]+str(c["experts_weights"])+version_name+c["special_name_agent"]+paths_config["model_ext"]
    non_eth_expert = paths_config["data_path"]+c["env"]+"/"+vanilla_path+str(c["non_eth_experts_weights"])+"/"+paths_config["expe_path"]+paths_config["model_ext"]
    return moral_filename, non_eth_expert